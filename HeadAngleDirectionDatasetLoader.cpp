#include "HeadAngleDirectionDatasetLoader.h"
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>
#include <dlib/image_processing.h>
#include <dlib/gui_widgets.h>
#include <dlib/image_io.h>
#include <iostream>

HeadAngleDirectionDatasetLoader::HeadAngleDirectionDatasetLoader(
        const string& images_folder) {
    this->rootDirecotry = images_folder;
    this->trainerIndex = 0;
    this->testIndex = 0;
    load_train_files_list();
}

void HeadAngleDirectionDatasetLoader::load_train_files_list() {

    image_info temp;
    temp.numeric_label = 0;
    // We will loop over all the label types in the dataset, each is contained in a subfolder.
    auto subdirs = directory(this->rootDirecotry).get_dirs();
    // But first, sort the sub directories so the numeric labels will be assigned in sorted order.
    sort(subdirs.begin(), subdirs.end());
    int index = 0;
    frontal_face_detector detector = get_frontal_face_detector();
    shape_predictor pose_model;
    deserialize("shape_predictor_68_face_landmarks.dat") >> pose_model;
    image_window win, win_faces;
    long minX = 1000 , minY = 1000, maxX = 0, maxY = 0;
    dpoint a = dpoint(0,0);
    for (auto subdir : subdirs) {
        // Now get all the images in this label type
        for (std::string image_file : subdir.get_files()) {
            if (image_file.find(".txt") == string::npos) {
                cout << "processing image " << image_file << endl;
                array2d<unsigned char> img;
                load_image(img, image_file);
                pyramid_up(img);
                std::vector<rectangle> dets = detector(img);
                std::vector<full_object_detection> shapes;
                for (unsigned long i = 0; i < dets.size(); ++i){
            
                    auto shape = pose_model(img, dets[i]);
                    // Left eye
                     for (unsigned long i = 37; i <= 41; ++i) {
                        
                        long x = shape.part(i).x();
                        long y = shape.part(i).y();
                        if(minX > x){
                            minX = x;
                        }
                        if(maxX < x) {
                            maxX = x;
                        }
                        if(minY > y){
                            minY = y;
                        }
                        if(maxY < y) {
                            maxY = y;
                        }
                     
                     }
                    // Right eye
                    for (unsigned long i = 43; i <= 47; ++i){
                        long x = shape.part(i).x();
                        long y = shape.part(i).y();
                        if(minX > x){
                            minX = x;
                        }
                        if(maxX < x) {
                            maxX = x;
                        }
                        if(minY > y){
                            minY = y;
                        }
                        if(maxY < y) {
                            maxY = y;
                        }
                    }
                    shapes.push_back(shape);
                }
               // dpoint(minX)
                cout << "Crate cut image minX " << minX << " min y " << minY << " maxX: " << maxX << " maxY " << maxY << endl;
                array2d<unsigned char> imgCut = array2d<unsigned char>(maxY-minY, maxX-minX);
                std::array<dpoint,4> array;
                array[0] = dpoint(minX,minY);
                array[2] = dpoint(minX, maxY);
                array[1] = dpoint(maxX,maxY);
                array[3] = dpoint(maxX, minY);
                extract_image_4points(img, imgCut, array);
                dlib::array<array2d<rgb_pixel> > face_chips;
                //extract_image_chips(img, get_face_chip_details(shapes), face_chips);
                win_faces.set_image(imgCut);
                cout << "Number of faces detected: " << dets.size() << endl;
                win.clear_overlay();
                win.set_image(img);
                win.add_overlay(render_face_detections(shapes));
                //person00100-90+45.jpg
                string baseImageName = base_name(image_file);
                win.add_overlay(dets, rgb_pixel(255, 0, 0));
                cout << "Hit enter to process the next image..." << endl;
                cin.get();
                int start = 11;
                int end = baseImageName.length() - start - 4;
                string labelName = baseImageName.substr(start, end);
                position p = getHeadPozitionFromString(labelName);
                labelName = parseLabel(p);
                //cout << baseImageName << " -> " << labelName << endl;
                temp.label = labelName;
                temp.filename = image_file;
                if (this->datasetLabelsInfo.find(labelName)
                        == this->datasetLabelsInfo.end()) {
                    this->datasetLabelsInfo[labelName] = {index, 0};
                    temp.numeric_label = index;
                    index++;
                } else {
                    temp.numeric_label = this->datasetLabelsInfo.find(labelName)->second.numeric_label;
                    this->datasetLabelsInfo.find(labelName)->second.count++;
                }

                if (datasetLabelsInfo.find(labelName)->second.count % 10 == 0) {
                    datasetTestImagesInfo.push_back(temp);
                } else {
                    datasetTrainerImagesInfo.push_back(temp);
                }

            }
        }
    }
    /*
    for (auto set : datasetLabelsInfo){
            cout << set.first << set.second.count<<endl;
    }
     */
}

position HeadAngleDirectionDatasetLoader::getHeadPozitionFromString(
        std::string const & str) {
    position pos;
    size_t minusCharPos = str.find("-", 1);
    size_t addCharPos = str.find("+", 1);
    int separator;
    if (addCharPos == string::npos) {
        separator = minusCharPos;
    } else {
        separator = addCharPos;
    }
    string h = str.substr(separator);
    string v = str.substr(0, separator);
    pos.horizontal = stod(h);
    pos.vertica = stod(v);
    return pos;
}

std::string HeadAngleDirectionDatasetLoader::parseLabel(position pos) {
    int v = pos.vertica;
    int h = pos.horizontal;
    string ret = "";
    if (v == 0 && h == 0) {
        return "front";
    }
    if (v < 0) {
        ret += "left";
    } else if (v > 0) {
        ret += "right";
    }

    if (h < 0) {
        ret += "down";
    } else if (h > 0) {
        ret += "top";
    }
    return ret;
}

std::string HeadAngleDirectionDatasetLoader::base_name(
        std::string const &path) {
    return path.substr(path.find_last_of("/\\") + 1);
}

dataset HeadAngleDirectionDatasetLoader::getNextTrainigImages() {
    dataset ret;
    int start = this->trainerIndex;
    int end = this->trainerIndex + DEFAULT_IMAGE_SET_COUNT;
    int filesCount = this->getTrainFilesCount();
    if (end >= filesCount) {
        end = filesCount;
    }
    //cout << "Load trainer images from " << start << " to " << end << endl;
    for (int i = start; i < end - 1; i++) {
        //cout<<i<<endl;
        image_info current = this->datasetTrainerImagesInfo[i];
        matrix<rgb_pixel> img;
        load_image(img, current.filename);
        ret.images.push_back(img);
        ret.labels.push_back(current.numeric_label);
    }
    this->trainerIndex = end;
    return ret;
}

dataset HeadAngleDirectionDatasetLoader::getAllTestingImages() {
    dataset ret;
    for (image_info current : this->datasetTestImagesInfo) {
        matrix<rgb_pixel> img;
        load_image(img, current.filename);
        ret.images.push_back(img);
        ret.labels.push_back(current.numeric_label);
    }
    return ret;
}

dataset HeadAngleDirectionDatasetLoader::getAllTrainigImages() {
    dataset ret;
    for (image_info current : this->datasetTrainerImagesInfo) {
        matrix<rgb_pixel> img;
        load_image(img, current.filename);
        ret.images.push_back(img);
        ret.labels.push_back(current.numeric_label);
    }
    return ret;
}

int HeadAngleDirectionDatasetLoader::getTestFilesCount() {
    return this->datasetTestImagesInfo.size();
}

int HeadAngleDirectionDatasetLoader::getTrainFilesCount() {
    return this->datasetTrainerImagesInfo.size();
}

int HeadAngleDirectionDatasetLoader::getLabelsCount() {
    return this->datasetLabelsInfo.size();
}

bool HeadAngleDirectionDatasetLoader::datasetIsEmpty() {
    return this->trainerIndex >= this->getTrainFilesCount();
}

