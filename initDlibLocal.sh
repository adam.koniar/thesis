#!/bin/bash

if [ -d lib/dlib ]
then
	echo 'Delete prew instalation'
	rm -rf lib/dlib
fi

if [ -d dlib-src ]
then
	echo 'Delete prew source'
	rm -rf dlib
fi

git clone https://github.com/davisking/dlib.git -b v19.16 dlib-src

cd dlib-src
mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=../../lib/dlib
cmake --build . --config Release
make install

cd ../..

rm -rf dlib-src


