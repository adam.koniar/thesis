#include <dlib/dnn.h>
#include <dlib/data_io.h>
#include <string>
#include <iostream>
#include <algorithm>
#include <vector>
#include <map>

using namespace std;
using namespace dlib;

struct image_info {
    string filename;
    string label;
    long numeric_label;
    bool setAsTest;
};

struct label_info {
    long numeric_label;
    long count;
};

struct dataset{
	std::vector<dlib::matrix<rgb_pixel>> images;
	std::vector<unsigned long> labels;
};

struct position{
	double vertica;
	double horizontal;
};

class HeadAngleDirectionDatasetLoader{
    public:
    dataset getNextTrainigImages();
    dataset getAllTestingImages();
    dataset getAllTrainigImages();
    HeadAngleDirectionDatasetLoader(const string& images_folder);
    int getTrainFilesCount();
    int getTestFilesCount();
    int getLabelsCount();
    bool datasetIsEmpty();

    protected:
    static std::string base_name(std::string const & path);
    static position getHeadPozitionFromString(std::string const & str);
    static std::string parseLabel(position pos);

    private:
	const int DEFAULT_IMAGE_SET_COUNT = 22;
	long trainerIndex;
	long testIndex;
    HeadAngleDirectionDatasetLoader();
    std::map<string,label_info> datasetLabelsInfo;
    std::vector<image_info> datasetTrainerImagesInfo;
    std::vector<image_info> datasetTestImagesInfo;
    string rootDirecotry;
    void load_train_files_list();
};
