#include <dlib/dnn.h>
#include <iostream>
#include <dlib/data_io.h>
#include "HeadAngleDirectionDatasetLoader.h"
#include <string>
#include <map>

using namespace std;
using namespace dlib;

// Define core block
// Here we have parameterized the "block" layer on a BN layer (nominally some
// kind of batch normalization), the number of filter outputs N, and the stride
// the block operates at.
template <int N, template <typename> class BN, int stride, typename SUBNET>
using block  = BN<con<N,3,3,1,1,relu<BN<con<N,3,3,stride,stride,SUBNET>>>>>;

// Next, we need to define the skip layer mechanism used in the residual network
// paper.  They create their blocks by adding the input tensor to the output of
// each block.  So we define an alias statement that takes a block and wraps it
// with this skip/add structure.

// Note the tag layer.  This layer doesn't do any computation.  It exists solely
// so other layers can refer to it.  In this case, the add_prev1 layer looks for
// the tag1 layer and will take the tag1 output and add it to the input of the
// add_prev1 layer.  This combination allows us to implement skip and residual
// style networks.  We have also set the block stride to 1 in this statement.
// The significance of that is explained next.
template <template <int,template<typename>class,int,typename> class block, int N, template<typename>class BN, typename SUBNET>
using residual = add_prev1<block<N,BN,1,tag1<SUBNET>>>;

// Some residual blocks do downsampling.  They do this by using a stride of 2
// instead of 1.  However, when downsampling we need to also take care to
// downsample the part of the network that adds the original input to the output
// or the sizes won't make sense (the network will still run, but the results
// aren't as good).  So here we define a downsampling version of residual.  In
// it, we make use of the skip1 layer.  This layer simply outputs whatever is
// output by the tag1 layer.  Therefore, the skip1 layer (there are also skip2,
// skip3, etc. in dlib) allows you to create branching network structures.

// residual_down creates a network structure like this:
/*
 input from SUBNET
 /     \
            /       \
         block     downsample(using avg_pool)
 \       /
 \     /
 add tensors (using add_prev2 which adds the output of tag2 with avg_pool's output)
 |
 output
 */

template <template <int,template<typename>class,int,typename> class block, int N, template<typename>class BN, typename SUBNET>
using residual_down = add_prev2<avg_pool<2,2,2,2,skip1<tag2<block<N,BN,2,tag1<SUBNET>>>>>>;

// Now we can define 4 different residual blocks we will use in this example.
// The first two are non-downsampling residual blocks while the last two
// downsample.  Also, res and res_down use batch normalization while ares and
// ares_down have had the batch normalization replaced with simple affine
// layers.  We will use the affine version of the layers when testing our
// networks.
template <int N, typename SUBNET> using res       = relu<residual<block,N,bn_con,SUBNET>>;
template <int N, typename SUBNET> using ares      = relu<residual<block,N,affine,SUBNET>>;
template <int N, typename SUBNET> using res_down  = relu<residual_down<block,N,bn_con,SUBNET>>;
template <int N, typename SUBNET> using ares_down = relu<residual_down<block,N,affine,SUBNET>>;

template <typename SUBNET> using level1 = res<512,res<512,res_down<512,SUBNET>>>;
template <typename SUBNET> using level2 = res<256,res<256,res<256,res<256,res<256,res_down<256,SUBNET>>>>>>;
template <typename SUBNET> using level3 = res<128,res<128,res<128,res_down<128,SUBNET>>>>;
template <typename SUBNET> using level4 = res<64,res<64,res<64,SUBNET>>>;

template <typename SUBNET> using alevel1 = ares<512,ares<512,ares_down<512,SUBNET>>>;
template <typename SUBNET> using alevel2 = ares<256,ares<256,ares<256,ares<256,ares<256,ares_down<256,SUBNET>>>>>>;
template <typename SUBNET> using alevel3 = ares<128,ares<128,ares<128,ares_down<128,SUBNET>>>>;
template <typename SUBNET> using alevel4 = ares<64,ares<64,ares<64,SUBNET>>>;

// Now that we have these convenient aliases, we can define a residual network
// without a lot of typing.  Note the use of a repeat layer.  This special layer
// type allows us to type repeat<9,res,SUBNET> instead of
// res<res<res<res<res<res<res<res<res<SUBNET>>>>>>>>>.  It will also prevent
// the compiler from complaining about super deep template nesting when creating
// large networks.
const unsigned long number_of_classes = 9;
// training network type
using net_type = loss_multiclass_log<fc<1000,avg_pool_everything<
                            level1<
                            level2<
                            level3<
                            level4<
                            max_pool<3,3,2,2,relu<bn_con<con<64,7,7,2,2,
							input_rgb_image
                            >>>>>>>>>>>;

// testing network type (replaced batch normalization with fixed affine transforms)
using anet_type = loss_multiclass_log<fc<1000,avg_pool_everything<
                            alevel1<
                            alevel2<
                            alevel3<
                            alevel4<
                            max_pool<3,3,2,2,relu<affine<con<64,7,7,2,2,
							input_rgb_image
                            >>>>>>>>>>>;


// And finally, let's define a residual network building block that uses
// parametric ReLU units instead of regular ReLU.
template<typename SUBNET>
using pres = prelu<add_prev1<bn_con<con<8,3,3,1,1,prelu<bn_con<con<8,3,3,1,1,tag1<SUBNET>>>>>>>>;

int main(int argc, char** argv)
try {

	 cout << "Scanning image dataset" << endl;
	HeadAngleDirectionDatasetLoader* loader =
			new HeadAngleDirectionDatasetLoader("datasets/head_angle");

	int testFilesCount = loader->getTestFilesCount();
	int trainFilesCount = loader->getTrainFilesCount();
	cout << "Count of image files:" << testFilesCount + trainFilesCount << endl;
	cout << "\tTrain files: " << trainFilesCount << endl;
	cout << "\tTest files: " << testFilesCount << endl;
	cout << "Count of labels: " << loader->getLabelsCount() << endl;
	cout << "End of scanning!" << endl;
	cout << "end" << endl;

        /*
	// dlib uses cuDNN under the covers.  One of the features of cuDNN is the
	// option to use slower methods that use less RAM or faster methods that use
	// a lot of RAM.  If you find that you run out of RAM on your graphics card
	// then you can call this function and we will request the slower but more
	// RAM frugal cuDNN algorithms.
	set_dnn_prefer_smallest_algorithms();

	// Create a network as defined above.  This network will produce 10 outputs
	// because that's how we defined net_type.  However, fc layers can have the
	// number of outputs they produce changed at runtime.
	net_type net;
	/*
	cout << "The pnet has " << net.num_layers << " layers in it." << endl;
	cout << net << endl;

	dnn_trainer<net_type, adam> trainer(net, adam(0.0005, 0.9, 0.999));
	trainer.be_verbose();
	trainer.set_iterations_without_progress_threshold(2000);
	trainer.set_learning_rate_shrink_factor(0.1);
	// The learning rate will start at 1e-3.
	trainer.set_learning_rate(1e-3);
	trainer.set_synchronization_file("mnist_resnet_sync", std::chrono::seconds(100));
	set_all_bn_running_stats_window_sizes(net, 1000);


    std::vector<matrix<rgb_pixel>> samples;
	int i = 100;
	while (!loader->datasetIsEmpty()) {
		cout << i << ":wtf ->";
		dataset trainingData = loader->getNextTrainigImages();
		trainer.train_one_step(trainingData.images, trainingData.labels);
		cout << "OK" <<endl;
		i++;
	}
	cout<< "I get here!" << endl;
	// When you call train_one_step(), the trainer will do its processing in a
	// separate thread.  This allows the main thread to work on loading data
	// while the trainer is busy executing the mini-batches in parallel.
	// However, this also means we need to wait for any mini-batches that are
	// still executing to stop before we mess with the net object.  Calling
	// get_net() performs the necessary synchronization.
	trainer.get_net();

	net.clean();
	serialize("mnist_res_network.dat") << net;

	*/
	// Now we have a trained network.  However, it has batch normalization
	// layers in it.  As is customary, we should replace these with simple
	// affine layers before we use the network.  This can be accomplished by
	// making a network type which is identical to net_type but with the batch
	// normalization layers replaced with affine.  For example:
	// Then we can simply assign our trained net to our testing net.
	/*anet_type tnet = net;
	// Or if you only had a file with your trained network you could deserialize
	// it directly into your testing network.
	//deserialize("mnist_res_network.dat") >> tnet;

	//deserialize("result/mnist_res_network.dat") >> tnet;

	// And finally, we can run the testing network over our data.
	dataset test_dataset = loader->getAllTestingImages();
	/*
	dataset train_dataset = loader->getAllTrainigImages();

	std::vector<unsigned long> predicted_labels = tnet(train_dataset.images);
	int num_right = 0;
	int num_wrong = 0;
	for (size_t i = 0; i < test_dataset.labels.size(); ++i) {
		if (predicted_labels[i] == train_dataset.labels[i])
			++num_right;
		else
			++num_wrong;

	}
	cout << "training num_right: " << num_right << endl;
	cout << "training num_wrong: " << num_wrong << endl;
	cout << "training accuracy:  "
			<< num_right / (double) (num_right + num_wrong) << endl;
	*/

	/*std::vector<unsigned long> predicted_labels = tnet(test_dataset.images);
	int num_right = 0;
	int num_wrong = 0;
	for (size_t i = 0; i < test_dataset.images.size(); ++i) {
		if (predicted_labels[i] == test_dataset.labels[i])
			++num_right;
		else
			++num_wrong;

	}
	cout << "testing num_right: " << num_right << endl;
	cout << "testing num_wrong: " << num_wrong << endl;
	cout << "testing accuracy:  "
			<< num_right / (double) (num_right + num_wrong) << endl;
        */

	return 0;
}
catch (std::exception& e) {
	cout << e.what() << endl;
}
